<!doctype html>
<html lang="en">
<head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title-block')</title>
        <link rel="stylesheet" href="/css/woocommerce-layout.css" type="text/css" media="all">
        <link rel="stylesheet" href="/css/woocommerce-smallscreen.css" type="text/css" media="only screen and (max-width: 768px)">
        <link rel="stylesheet" href="/css/woocommerce.css" type="text/css" media="all">
        <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css" media="all">
        <link rel="stylesheet" href="/css/style.css" type="text/css" media="all">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,500,700%7CRoboto:400,500,700%7CHerr+Von+Muellerhoff:400,500,700%7CQuattrocento+Sans:400,500,700" type="text/css" media="all">
        <link rel="stylesheet" href="/css/easy-responsive-shortcodes.css" type="text/css" media="all">

</head>
<body class="home page page-template page-template-template-portfolio page-template-template-portfolio-php">
<div id="page">
    <div class="container">
        <header id="masthead" class="site-header">
            <div class="site-branding">
                <h1 class="site-title"><a href="{{route('article.index')}}" rel="home">Moschino</a></h1>
                <h2 class="site-description">Minimalist Portfolio HTML Template</h2>
            </div>
            <nav id="site-navigation" class="main-navigation">
                <button class="menu-toggle">Menu</button>
                <a class="skip-link screen-reader-text" href="#">Skip to content</a>
                <div class="menu-menu-1-container">
                    <ul id="menu-menu-1" class="menu nav-menu">
                        <li><a href="{{route('article.index')}}">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Shop</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Elements</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Pages</a>
                            <ul class="sub-menu">
                                <li><a href="#">Portfolio Item</a></li>
                                <li><a href="#">Blog Article</a></li>
                                <li><a href="#">Shop Item</a></li>
                                <li><a href="#">Portfolio Category</a></li>
                            </ul>
                        </li>
                       @guest
                        <li><a href="{{route('login')}}">Login</a></li>
                        <li><a href="{{route('register')}}">Registry</a></li>
                        @endguest

                        @auth
                            <li><a href="{{route('article.create')}}">Create article</a></li>
                            <li>
                       <form method="post" action="{{route('logout')}}">
                           @csrf
                           <button type="submit">Logout</button>
                       </form>
                            </li>
                        @endauth
                    </ul>
                </div>
            </nav>
        </header>
        <!-- #masthead -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area column full">
                <main id="main" class="site-main">
        @yield('content')
            </main>
            </div>
        </div>
        <!-- #content -->
    </div>
    <!-- .container -->
    <footer id="colophon" class="site-footer">
        <div class="container">
            <div class="site-info">
                <h1 style="font-family: 'Herr Von Muellerhoff';color: #ccc;font-weight:300;text-align: center;margin-bottom:0;margin-top:0;line-height:1.4;font-size: 46px;">Moschino</h1>
                <a target="blank" href="https://www.wowthemes.net/">© Moschino - Free HTML Template by WowThemes.net</a>
            </div>
        </div>
    </footer>
    <a href="#top" class="smoothup" title="Back to top"><span class="genericon genericon-collapse"></span></a>
</div>
<!-- #page -->
<script src="js/jquery.js"></script>
<script src="js/plugins.js"></script>
<script src="js/scripts.js"></script>
<script src="js/masonry.pkgd.min.js"></script>

</body>
</html>
