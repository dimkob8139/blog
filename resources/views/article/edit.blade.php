@extends('layouts.master')

@section('title-block')
    Create article
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <article>
        <form method="POST" action="{{route('article.update')}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="id" value="{{$article->id}}">
            <header class="entry-header">
                <h1 class="entry-title">Edit article</h1><br>
                @error('title')
                <div>{{ $message }}</div>
                @enderror
                <h1 class="entry-title"><input type="text" name="title" value="{{$article->title}}">title</h1>
                <h1 class="entry-title"><input type="text" name="image" value="{{$article->image}}">image</h1>
                <div class="entry-thumbnail">
                  <a href=""><img src="/public/storage/{{$article->image}}"></a>
                </div>
                <div class="entry-thumbnail">
                    <h1 class="entry-title">Upload image</h1>
                    <input type="file" name="banner">
                </div>
                @error('file')
                <div>{{ $message }}</div>
                @enderror
            </header>
            <!-- .entry-header -->
            <div class="entry-content">
                @error('content')
                <div>{{ $message }}</div>
                @enderror
                <label>
                    <textarea rows="6" name="description">{{$article->description}}</textarea>
                </label>
            </div>
            <div class="entry-content" style="display: flex;flex-direction: column">
                @foreach($tags as $tag)

                    <input type="checkbox" id="{{$tag->name}}" name="tags[]" value="{{$tag->id}}">
                    <label for="{{$tag->name}}">{{$tag->name}}</label>
                @endforeach
            </div>
            <!-- .entry-content -->

            <!-- .entry-footer -->
            <button class="wpcmsdev-button color-yellow hentry" type="submit">Edit</button>
        </form>
    </article>
@endsection
