
@extends('layouts.master')

@section('content')

                <div class="grid portfoliogrid" style="position: relative; height: 200px;">
                 @foreach($articles as $article)
                    <article class="hentry" style="position: absolute; left: 0px; top: 0px;">
                        <header class="entry-header">
                            <div class="entry-thumbnail">
                                <img src="/storage/{{$article->image}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="p1">
                            </div>
                            <h2 class="entry-title">
                                <a>{{$article->title}}</a>

                            </h2>
                            <h2 class="entry-title">
                                <a href="" rel="bookmark">{{$article->description}}</a>
                            </h2>
                            @foreach($article->tags as $tag)
                                <a>{{$tag->name}}</a>
                            @endforeach
                            <br>
                            
                            <a href="{{route('article.edit',['id'=>$article->id])}}" rel="bookmark" style="color: green">Edit</a><br>
                            <a href="{{route('article.delete',['id'=>$article->id])}}" rel="bookmark" style="color: red">Delete</a>
                        </header>
                    </article><br>

                    @endforeach

                </div>

                {{ $articles->links() }}
                <!-- .grid -->

{{--                <nav class="pagination">--}}
{{--                    <span class="page-numbers current">1</span>--}}
{{--                    <a class="page-numbers" href="#">2</a>--}}
{{--                    <a class="next page-numbers" href="#">Next »</a>--}}
{{--                </nav>--}}
                <br>


    <!-- .container -->
@endsection
