@extends('layouts.master')

@section('title-block')
    Create article
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <article>
        <form method="POST" action="{{route('article.store')}}" enctype="multipart/form-data">
            @csrf
            <header class="entry-header">
                <h1 class="entry-title">Create new article</h1><br>
                @error('title')
                <div>{{ $message }}</div>
                @enderror
                <h1 class="entry-title"><input type="text" name="title" value="">title</h1>
                <h1 class="entry-title"><input type="text" name="image" value="">image</h1>
                <div class="entry-thumbnail">
                    <h1 class="entry-title">Upload image</h1>
                    <input type="file" name="banner">
                </div>
                @error('file')
                <div>{{ $message }}</div>
                @enderror
            </header>
            <!-- .entry-header -->
            <div class="entry-content">
                @error('content')
                <div>{{ $message }}</div>
                @enderror
                <label>
                    <textarea rows="6" name="description">Enter text</textarea>
                </label>
            </div>
           <div class="entry-content" style="display: flex;flex-direction: column">
                @foreach($tags as $tag)

                        <input type="checkbox" id="{{$tag->name}}" name="tags[]" value="{{$tag->id}}">
                         <label for="{{$tag->name}}">{{$tag->name}}</label>
                @endforeach
            </div>
            <!-- .entry-content -->

            <!-- .entry-footer -->
            <button class="wpcmsdev-button color-violet hentry" type="submit">Create</button>
        </form>
    </article>
@endsection
