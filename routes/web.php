<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

require __DIR__.'/auth.php';

Route::get('/',[HomeController::class,'index'])->name('home');

Route::middleware(['auth'])->group(function (){
    Route::prefix('/article')->name('article.')->group(function (){
          Route::get('/',[ArticleController::class,'index'])->name('index');
        Route::get('/form',[ArticleController::class,'create'])->name('create');
        Route::post('/',[ArticleController::class,'store'])->name('store');
        Route::get('/edit/{id}',[ArticleController::class,'edit'])->name('edit');
        Route::put('/',[ArticleController::class,'update'])->name('update');
        Route::get('/delete{id}',[ArticleController::class,'destroy'])->name('delete');
    });
});


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');





