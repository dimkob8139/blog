<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           // 'id' => ['required', 'integer'],

            'title'=>['required','string','max:50'],  //need add max lenght
            'image'=>['required','string','max:100'],  //need add max lenght
            'description'=>['required','string','max:255']
        ];
    }
}
