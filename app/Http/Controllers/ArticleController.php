<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Http\Requests\EditArticleRequest;
use App\Models\Article;
use App\Models\Tag;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=Auth::id();
        $articles=Article::with('tags')
            ->where('user_id',$id)
            ->simplePaginate(2);

         return view('article.index',['articles'=>$articles]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|
     * \Illuminate\Contracts\View\Factory|
     * \Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $tags=Tag::all();
        return view('article.create',['tags'=>$tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ArticleRequest $request,Article $article)
    {
        try {
            DB::beginTransaction();
            $user=Auth::user();
            $article->user_id=$user->id;
            $article->title=$request->title;
            $article->description=$request->description;
            $article->image=$request->file('banner')->store('banners');


            $article->save();
            $article->tags()->attach($request->tags);
            DB::commit();
        }
        catch (Exception $ex){
            Log::error($ex);
            DB::rollBack();
        }


      return redirect()->route('article.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags=Tag::all();
       $article=Article::findOrFail($id);
       return view('article.edit',['article'=>$article],['tags'=>$tags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request)
    {
        try {
            DB::beginTransaction();
            $article=Article::findOrFail($request->id);
            $userId=Auth::id();
            If($article->user_id!==$userId){
                abort(403);
            }
            if($request->hasFile('image')){
                $image=$request->file('image')->store('banners');
                $previousImage=$article->image;
                $article->image=$image;

            }
            $article->title=$request->title;
            $article->description=$request->description;
            $article->save();
            if(isset($previousImage)){
                Storage::delete($previousImage);
            }
            $article->tags()->sync($request->tags);
            DB::commit();
        }  catch (Exception $ex){
            Log::error($ex);
            DB::rollBack();
        }

        return redirect(route('article.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $article=Article::find($id);
            $userId=Auth::id();
            If($article->user_id!==$userId) {
                abort(403);
            }
            $article=Article::destroy($id);
            DB::commit();

        }
        catch (Exception $ex){
            Log::error($ex);
            DB::rollBack();
        }
        return redirect(route('article.index'));
    }
}
